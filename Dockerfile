FROM golang:1.13.1

LABEL maintaner="Andrzej Kaczynski <andrzej.kaczynski@container-solutions.com>"

WORKDIR /app

COPY ./ .

EXPOSE 8080

CMD ["./main"]
